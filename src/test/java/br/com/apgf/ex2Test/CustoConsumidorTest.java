
package br.com.apgf.ex2Test;

import br.com.apgf.ex2.CalculoConsumidor;
import br.com.apgf.ex2.Veiculo;
import org.junit.Test;
import static org.junit.Assert.*;

public class CustoConsumidorTest {
    
    @Test
    public void deveMostrarOCustoDoConsumidor(){
        
        Veiculo veiculo = new Veiculo(1000);
        CalculoConsumidor Calculadora = new CalculoConsumidor();
        double resultado = Calculadora.tabelaCusto(veiculo);
        assertEquals(1730, resultado, 0.001);
    }
}
