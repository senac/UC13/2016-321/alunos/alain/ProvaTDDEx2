
package br.com.apgf.ex2;

import java.util.Scanner;


public class LeitorConsumidor {
    
    public static void main(String... a){
        
        Veiculo veiculo ; 
        
        double custoConsumidor ; 
        double custoFabrica ; 
                
        Scanner scanner = new Scanner(System.in) ; 
        
        System.out.println("Digite o CustoFabrica:");
        custoFabrica = scanner.nextDouble(); 
                       
        veiculo = new Veiculo (custoFabrica);
        CalculoConsumidor calculo = new CalculoConsumidor(); 
        custoConsumidor = calculo.tabelaCusto(veiculo);
        
        System.out.println("O Custo do Consumidor:" + custoConsumidor);
               
    }
    
}
