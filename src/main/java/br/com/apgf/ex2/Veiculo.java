
package br.com.apgf.ex2;

public class Veiculo {
    
    private double custoFabrica;
    private double custoConsumidor;
    
    public Veiculo() {
    }

    public Veiculo(double custoFabrica) {
        this.custoFabrica = custoFabrica;
    }   

    public double getCustoConsumidor() {
        return custoConsumidor;
    }

    public void setCustoConsumidor(double custoConsumidor) {
        this.custoConsumidor = custoConsumidor;
    }
    
    public double getCustoFabrica() {
        return custoFabrica;
    }

    public void setCustoFabrica(double custoFabrica) {
        this.custoFabrica = custoFabrica;
    }
    
    public double percentualDistribuidor(){
        return this.custoFabrica * 0.28;
    }
    
    public double impostos(){
        return this.custoFabrica * 0.45; 
    }
}    