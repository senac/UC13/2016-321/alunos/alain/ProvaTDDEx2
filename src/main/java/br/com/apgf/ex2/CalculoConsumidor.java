
package br.com.apgf.ex2;

public class CalculoConsumidor {
    
    private double resultado;
    
    public double tabelaCusto(Veiculo veiculo){
        
        
        resultado = veiculo.getCustoFabrica() + 
                veiculo.percentualDistribuidor() +
                veiculo.impostos();
        
        return resultado;
        
    }
}
